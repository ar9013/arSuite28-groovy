package com.mygdx.game.android;

import android.os.Bundle
import android.support.multidex.MultiDex;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import com.mygdx.game.Drop;


public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MultiDex.install(this);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new Drop(), config);
	}
}
